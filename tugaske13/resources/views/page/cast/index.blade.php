@extends('layout.master')
@section('title')
    Halaman Data Table
@endsection
@section('sub-title')
   <a href="{{url('cast/create')}}" class="btn btn-primary">+ Tambah Data</a>
@endsection
@section('content')
@push('script')
    
    <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>

@endpush
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th class="col-1">No</th>
      <th class="col-4">Nama</th>
      <th>Umur</th>
      <th class="col-3">Aksi</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach ($data as $item)
    <tr>
      <td>{{$i++}}</td>
      <td>{{$item->nama}}</td>
      <td>{{$item->umur}} Tahun</td>
      <td><a href="{{route('cast.edit', $item->id)}}" class=" btn btn-sm btn btn-warning">Update</a>
        <form onsubmit="return confirm('Yakin mau untuk menghapus data ini?')" action="{{route('cast.destroy', $item->id)}}" class="d-inline" method="POST">
          @csrf
          @method('DELETE')
          <button class="btn btn-sm btn-danger" type="submit" name="submit">Delete</button>
      </form>
      </td>
    </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Aksi</th>
    </tr>
    </tfoot>
  </table>
@endsection