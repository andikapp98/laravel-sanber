@extends('layout.master')
@section('title')
    Cast
@endsection
@section('sub-title')
    Update Cast
@endsection
@section('content')
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('cast.update', $data->id)}}" id="quickForm" method="POST">
                @method('PUT')
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama </label>
                    <input type="text" name="nama" class="form-control" id="" placeholder="Masukkan Nama" value="{{$data->nama}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Umur</label>
                    <input type="number" name="umur" class="form-control" id="" placeholder="Masukkan Tahun" value="{{$data->umur}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Biodata</label>
                    <textarea class="form-control summernote" rows="5" name="bio" value="">{{$data->bio}}</textarea>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection