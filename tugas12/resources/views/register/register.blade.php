<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{url('/welcome')}}">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label for="">First name:</label><br><br>
         <input type="text" name="firstname" ><br><br>
        <label for="">Last name:</label><br><br>
        <input type="text" name="lastname" id=""><br><br>
        <label for="">Gender:</label><br><br>
        <input type="Radio">Male<br>
        <input type="Radio">Female<br>
        <input type="Radio">Other<br><br>
        <label for="">Nationality:</label><br><br>
        <select name="" id="">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
        </select><br><br>
        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" name="" id="">Indonesia<br>
        <input type="checkbox" name="" id="">English<br>
        <input type="checkbox" name="" id="">Other<br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        <button>Sign Up</button>
    </form>
</body>
</html>