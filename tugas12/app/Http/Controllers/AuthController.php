<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register(){ 
        return view('register.register');
    } 

    function welcome(Request $request){

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        return view('welcome.index')->with(['firstname' => $firstname, 'lastname'=>$lastname]);
    }
}
